# deGAUSS Daily Weather

A repo to build a container for running [Degauss daily weather](https://degauss.org/narr/) to obtain daily weather variables for a date range for geocoded locations. The container is bootstrapped from the ['narrr' image](https://github.com/degauss-org/narr), can run RStudio Server and has additional tools including tidyverse and the 'sf' R package.

## Building

- A new container build will only be initiated by a modification to the Singularity.def file
- The built container file will only be pushed to the Duke OIT registry when a commit is tagged (ideally following the current version naming convention)
    + Note that to push, the container must be re-built first.

## Pulling

To pull the latest version of the container file via Singularity, use the following command where "/path/to/container/directory" is the directory where the container will be pulled to:

```
singularity pull --force --dir /path/to/container/directory oras://gitlab-registry.oit.duke.edu/chart-consortium/geospatial/degauss-daily-weather/degauss-daily-weather:latest
```

Alternatively, the latest container version can be pulled using wget (to the current directory by default):

```
wget --no-check-certificate https://research-singularity-registry-public.oit.duke.edu/chart-consortium/degauss-daily-weather.sif
```

## Running

The container can be run using something like the following, where "path to geocoded addresses" is the path to CSV file with geocoded addresses and with start_date and end_date columns:

```
export ADDRESS_FILE="path to geocoded addreses"
singularity run degauss-daily-weather.sif ${ADDRESS_FILE}
```

## Notes



